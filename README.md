# About #

This is a toy implementation of blockchain. It is not intended for anything other than a laugh.

### About ###

A blockchain is a simple, yet powerful structure where a block of data is chained together with other blocks in such a way that the order of block additions is assured. This is accomplished by a new block storing the signature (hash) of the previous block, and its signature including the hash of the previous block to link those two blocks together. In this implementation the signature is the hash of the block's data, and the signature of its ancestor.

### Consensus ###

This toy does not provide a consensus mechanism which makes it useless. Blockchain itself isn't that useful beyond ensuring that the chain of blocks have not been tampered with. The consensus mechanism is how a blockchain can ensure that it adds blocks accurately in a zero trust environment. In other environments there may be other considerations beyond trust.

### What's next? ###

Probably a dog meme network

### Example ###
```
pepeca-mp:toy-blockchain pepeca$ python toy_blockchain.py
I am a new chain. Creating genesis block
proposing block I am the beginning. Not special, just first, [0]
proposing block this is block 1, [1, 2, 3]
Added d83d240023036806b49962796394ff76 to chain with ancestor 5aa1aeec8af75ae52cbbb97b7f0e166f
proposing block this is block 2, [4, 5, 6]
Added 096f4876edb3626525d8237896b8d189 to chain with ancestor d83d240023036806b49962796394ff76
proposing block this is block 3, [7, 8, 9]
Added 4eba782fbb1a515ea2899cc5074ad3c0 to chain with ancestor 096f4876edb3626525d8237896b8d189
proposing block this is block 4, [10, 11]
Added 22e2f9f6c051d42292d95a36a4eda3bd to chain with ancestor 4eba782fbb1a515ea2899cc5074ad3c0
-- verifying the block chain --
ancestor: 5aa1aeec8af75ae52cbbb97b7f0e166f, signature: d83d240023036806b49962796394ff76, message: this is block 1
[1] verify ancestors: 5aa1aeec8af75ae52cbbb97b7f0e166f == 5aa1aeec8af75ae52cbbb97b7f0e166f
[1] verify signatures match hash: d83d240023036806b49962796394ff76 == d83d240023036806b49962796394ff76
ancestor: d83d240023036806b49962796394ff76, signature: 096f4876edb3626525d8237896b8d189, message: this is block 2
[2] verify ancestors: d83d240023036806b49962796394ff76 == d83d240023036806b49962796394ff76
[2] verify signatures match hash: 096f4876edb3626525d8237896b8d189 == 096f4876edb3626525d8237896b8d189
ancestor: 096f4876edb3626525d8237896b8d189, signature: 4eba782fbb1a515ea2899cc5074ad3c0, message: this is block 3
[3] verify ancestors: 096f4876edb3626525d8237896b8d189 == 096f4876edb3626525d8237896b8d189
[3] verify signatures match hash: 4eba782fbb1a515ea2899cc5074ad3c0 == 4eba782fbb1a515ea2899cc5074ad3c0
ancestor: 4eba782fbb1a515ea2899cc5074ad3c0, signature: 22e2f9f6c051d42292d95a36a4eda3bd, message: this is block 4
[4] verify ancestors: 4eba782fbb1a515ea2899cc5074ad3c0 == 4eba782fbb1a515ea2899cc5074ad3c0
[4] verify signatures match hash: 22e2f9f6c051d42292d95a36a4eda3bd == 22e2f9f6c051d42292d95a36a4eda3bd
-- corrupting data in the block chain --
ancestor: 5aa1aeec8af75ae52cbbb97b7f0e166f, signature: d83d240023036806b49962796394ff76, message: this is block 1
[1] verify ancestors: 5aa1aeec8af75ae52cbbb97b7f0e166f == 5aa1aeec8af75ae52cbbb97b7f0e166f
[1] verify signatures match hash: d83d240023036806b49962796394ff76 == d83d240023036806b49962796394ff76
ancestor: d83d240023036806b49962796394ff76, signature: 096f4876edb3626525d8237896b8d189, message: nothing to see here, totally valid transaction
[2] verify ancestors: d83d240023036806b49962796394ff76 == d83d240023036806b49962796394ff76
[2] verify signatures match hash: 6fb4cf49407ff690dcec3a0e014183bb == 096f4876edb3626525d8237896b8d189
Traceback (most recent call last):
  File "toy_blockchain.py", line 90, in <module>
    cs.scan(chain.chain)
  File "toy_blockchain.py", line 71, in scan
    assert( verifyChain[i].calculateSignature() == verifyChain[i].signature  )
AssertionError
```
