import hashlib
from pprint import pprint

class Block(object):
    def __init__(self, data):
        print("proposing block %s" % (data))
        self.ancestor = None
        self.data = data
        self.signature = "invalid"
        #TODO: does the sig need to be stored separetly from the calculation of sig to ensure no tampering has occurred?
        #I think so. When the block is added the sig is calculated, then a later process can verify the chain with sig == calculated sig

    def __repr__(self):
        return "ancestor: %s, signature: %s, message: %s" % (self.ancestor, self.signature, self.data.message)

    def calculateSignature(self):
        return hashlib.md5( "%s %s" % (self.data, self.ancestor)).hexdigest()

class Chain(object):
    def __init__(self):
        print("I am a new chain. Creating genesis block")
        self.chain = list()
        genesis = Block( Data(data=[0], message="I am the beginning. Not special, just first")) #ancestor = None,
        genesis.signature = hashlib.md5( "%s %s" % (genesis.data, "1") ).hexdigest()
        self.chain.append(genesis)

    def getAncestor(self):
        """
        genesis is hardcoded and doesn't use this. Any block with 0 ancestor hash is an orphan
        """
        if len(self.chain) > 0:
            return self.chain[-1].signature
        else:
            return 0

    def addBlock(self, block):
        """
        before adding a proposed block ensure the community supports the change
        TODO: setting ancestor and adding a new block would need to be protected to avoid collisions
        """
        if self.consensus():
            block.ancestor = chain.getAncestor()
            block.signature = block.calculateSignature()
            self.chain.append( block )
            print("Added %s to chain with ancestor %s" % (block.signature, block.ancestor))

    def consensus(self):
        """
        This is the important part of the implementation and needs to cover
        zero-trust consensus in most cases.
        """
        return True

class Data(object):
    def __init__(self, data, message):
        self.data = data
        self.message = message

    def __repr__(self):
        return "%s, %s" % (self.message, self.data)

class ChainScanner(object):
    def __init__(self):
        pass
    def scan(self, verifyChain):
        for i in range(1, len(verifyChain)):
            print(verifyChain[i])
            print("[%s] verify ancestors: %s == %s" % (i, verifyChain[i].ancestor, verifyChain[i-1].signature))
            assert( verifyChain[i].ancestor == verifyChain[i-1].signature  )
            print("[%s] verify signatures match hash: %s == %s" % (i, verifyChain[i].calculateSignature(), verifyChain[i].signature ))
            assert( verifyChain[i].calculateSignature() == verifyChain[i].signature  )

if __name__ == "__main__":
    chain = Chain()
    b1 = Block(Data(data=[1,2,3], message="this is block 1"))
    chain.addBlock(b1)
    b2 = Block(Data(data=[4,5,6], message="this is block 2"))
    chain.addBlock(b2)
    b3 = Block(Data(data=[7,8,9], message="this is block 3"))
    chain.addBlock(b3)
    b4 = Block(Data(data=[10,11], message="this is block 4"))
    chain.addBlock(b4)
    #verify the Chain
    print("-- verifying the block chain --")
    cs = ChainScanner()
    cs.scan(chain.chain)
    #cheat
    print("-- corrupting data in the block chain --")
    chain.chain[2].data = Data(data=[14,15,16], message="nothing to see here, totally valid transaction")
    cs.scan(chain.chain)
